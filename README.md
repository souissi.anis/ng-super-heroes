# Super Heroes

This is an Angular application to browse Marvel super heroes.  
You can search and see details of super heroes, and if you sign in, you can add them to your favorite list.  
This list is saved and when you log in, you can see it.

Follow this link for a live demo: https://ng-super-heroes.firebaseapp.com

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.0.

## Requirements

If you want to run this application on your server, you'll need to:  
1. go to [Marvel developer web site](https://developer.marvel.com/) and create an account
2. go to your account page and save your public and private keys somewhere (you'll need them later)  
![Marvel keys](https://firebasestorage.googleapis.com/v0/b/ng-super-heroes.appspot.com/o/marvel.jpg?alt=media&token=61585c24-1034-47c0-9c8d-20b8bcc60034)
3. go to [Firebase](https://firebase.google.com/) and create a project
4. get your Firebase project configuration by clicking on the "Add Firebase to your web app" from the home Overview screen of your Firebase console
![Firebase project configuration](https://firebasestorage.googleapis.com/v0/b/ng-super-heroes.appspot.com/o/firebase_conf.png?alt=media&token=c8268997-f1f8-4c2f-9851-dae40438a6a5)
3. install [yarn](https://yarnpkg.com/en/docs/install) (you can use npm if you prefer)

## Development server

```bash
# clone this repository 
git clone https://gitlab.com/souissi.anis/ng-super-heroes.git

# go to the ng-super-heroes folder
cd ng-super-heroes

# Install the dependencies
yarn

# create local.config file from local.template.config
cp ./src/environments/local.template.config ./src/environments/local.config

# !!! MANUAL STEP !!!
# open local.config in your editor and initialise variables 
# with your Marvel and Firebase configuration keys
# 
# replace '/' by '\/'
# 

# run config.sh to create src/environments/environment.ts 
# and initialise it with the variables you setup in previous step
#
# you'll probably need to run 'chmod +x ./config.sh' to add execute
# permission
#

./config.sh

# start dev server
yarn run start
```  
  
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## References
- ngrx example app:
  * https://github.com/ngrx/platform/tree/master/example-app
- Configuring CI/CD pipe on gitlab:
  * https://hackernoon.com/from-zero-to-production-with-angular-firebase-and-gitlab-ci-598181cfc6e5
  * https://juristr.com/blog/2018/02/cd-gitlab-angular-firebase/
- Firebase authentication integration:
  * https://itnext.io/step-by-step-complete-firebase-authentication-in-angular-2-97ca73b8eb32
- Firebase Firestore integration:
  * https://blog.thecodecampus.de/getting-started-firestore-angularfire2
- Server Side Rendring (SSR) with firebase:
  * https://hackernoon.com/deploy-angular-universal-w-firebase-ad70ea2413a1

## Todos
- [ ] Add unit tests
- [ ] Enable SSR again (waiting for angularfire2 to support SSR)
- [ ] Fix some issues with Firebase authentication
- [ ] Display more details about super heroes
