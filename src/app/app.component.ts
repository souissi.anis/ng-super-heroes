import {
  Component,
  ChangeDetectorRef,
  OnDestroy,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from './reducers';
import * as fromAuth from './auth/reducers';
import * as layout from './core/actions/layout';
import * as auth from './auth/actions/auth';

@Component({
  selector: 'app-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
  mobileQuery: MediaQueryList;

  isMobile$: Observable<boolean>;
  loggedIn$: Observable<boolean>;

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private store: Store<fromRoot.State>
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');

    this._mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      store.dispatch(new layout.SetIsMobile(this.mobileQuery.matches));
    };

    this.mobileQuery.addListener(this._mobileQueryListener);

    this.isMobile$ = store.pipe(select(fromRoot.getIsMobile));
    this.loggedIn$ = store.pipe(select(fromAuth.getLoggedIn));
  }

  logOut() {
    this.store.dispatch(new auth.Logout());
  }

  ngOnInit(): void {
    this.store.dispatch(new layout.SetIsMobile(this.mobileQuery.matches));
    this.store.dispatch(new auth.InitUser());
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
