import { AuthActions, AuthActionTypes } from './../actions/auth';
import { User } from '../models/auth';

export interface State {
  loggedIn: boolean;
  user: User | null;
  returnUrl: string;
}

export const initialState: State = {
  loggedIn: false,
  user: null,
  returnUrl: '/'
};

export function reducer(state = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        loggedIn: true,
        user: action.payload.user,
      };
    }

    case AuthActionTypes.LoginRedirect: {
      return {
        ...state,
        returnUrl: action.payload
      };
    }

    case AuthActionTypes.Logout: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getLoggedIn = (state: State) => state.loggedIn;
export const getUser = (state: State) => state.user;
export const getReturnUrl = (state: State) => state.returnUrl;
