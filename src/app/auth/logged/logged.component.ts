import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../models/auth';

@Component({
  selector: 'app-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.css']
})
export class LoggedComponent implements OnInit {
  @Input() user: User;
  @Output() logout = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
