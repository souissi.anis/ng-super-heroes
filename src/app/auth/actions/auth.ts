import { Action } from '@ngrx/store';
import { User, Authenticate, AuthProviderEnum } from '../models/auth';

export enum AuthActionTypes {
  InitUser = '[Auth] Initialize user if he is logged in',
  LoginWithEmail = '[Auth] Login with email',
  LoginWithProvider = '[Auth] Login with provider',
  Logout = '[Auth] Logout',
  LoginSuccess = '[Auth] Login Success',
  LoginFailure = '[Auth] Login Failure',
  LoginRedirect = '[Auth] Login Redirect',
}

export class InitUser implements Action {
  readonly type = AuthActionTypes.InitUser;
}

export class LoginWithEmail implements Action {
  readonly type = AuthActionTypes.LoginWithEmail;

  constructor(public payload: Authenticate) {}
}

export class LoginWithProvider implements Action {
  readonly type = AuthActionTypes.LoginWithProvider;

  constructor(public payload: AuthProviderEnum) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: { user: User, withRedirect: boolean }) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;

  constructor(public payload: any) {}
}

export class LoginRedirect implements Action {
  readonly type = AuthActionTypes.LoginRedirect;

  constructor(public payload: string) {}
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export type AuthActions =
  | InitUser
  | LoginWithEmail
  | LoginWithProvider
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | Logout;
