import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import {
  User,
  Authenticate,
  AuthProviderEnum,
  AuthActionEnum
} from '../models/auth';
import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { map } from 'rxjs/operators';
import { AuthProvider } from '@firebase/auth-types';

@Injectable()
export class AuthService {
  constructor(private firebaseAuth: AngularFireAuth) {}

  getUser(): Observable<User> {
    return this.firebaseAuth.authState.pipe(
      map(credential => {
        if (credential) {
          credential = credential as firebase.User;
          return { displayName: credential.displayName, uid: credential.uid, photoUrl: credential.photoURL };
        } else {
          return null;
        }
      })
    );
  }

  signInWithProvider(providerEnum: AuthProviderEnum) {
    let provider: AuthProvider;

    switch (providerEnum) {
      case AuthProviderEnum.Google:
        provider = new firebase.auth.GoogleAuthProvider();
        break;
      case AuthProviderEnum.Twitter:
        provider = new firebase.auth.TwitterAuthProvider();
        break;
      case AuthProviderEnum.Facebook:
        provider = new firebase.auth.FacebookAuthProvider();
        break;

      default:
        break;
    }

    return this.firebaseAuth.auth.signInWithPopup(provider);
  }

  loginWithEmail(authForm: Authenticate): Observable<User> {
    let authPromise: Promise<any>;

    switch (authForm.action) {
      case AuthActionEnum.Login:
        authPromise = this.firebaseAuth.auth.signInWithEmailAndPassword(
          authForm.email,
          authForm.password
        );
        break;
      case AuthActionEnum.Register:
        authPromise = this.firebaseAuth.auth.createUserWithEmailAndPassword(
          authForm.email,
          authForm.password
        );
        break;

      default:
        break;
    }

    return Observable.fromPromise(authPromise).pipe(
      map(credential => {
        credential = credential as firebase.User;
        return { displayName: credential.displayName, uid: credential.uid, photoUrl: credential.photoURL };
      })
    );
  }

  loginWithProvider(providerEnum: AuthProviderEnum): Observable<User> {
    return Observable.fromPromise(this.signInWithProvider(providerEnum)).pipe(
      map(credential => {
        credential = credential.user as firebase.User;
        return { displayName: credential.displayName, uid: credential.uid, photoUrl: credential.photoURL };
      })
    );
  }

  logout() {
    return Observable.of(this.firebaseAuth.auth.signOut());
  }
}
