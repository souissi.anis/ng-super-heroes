import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Authenticate, AuthProviderEnum, AuthActionEnum } from '../models/auth';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  @Input()
  set pending(isPending: boolean) {
    if (isPending) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  @Input() errorMessage: string | null;
  @Output() submitted = new EventEmitter<Authenticate>();
  @Output() signInWith = new EventEmitter<AuthProviderEnum>();

  // In order to use enums in the template, we need to create a reference to it
  authProvider = AuthProviderEnum;
  authActionEnum = AuthActionEnum;

  hide = true;

  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    action: new FormControl('')
  });

  constructor() { }

  ngOnInit() {
  }

  setAction(action: AuthActionEnum) {
    this.form.patchValue({ 'action': action });
  }

  submit() {
    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }

}
