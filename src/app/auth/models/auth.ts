export interface Authenticate {
  email: string;
  password: string;
  action: AuthActionEnum;
}

export interface User {
  uid: string;
  displayName: string;
  photoUrl: string;
}

export enum AuthProviderEnum {
  Google,
  Twitter,
  Facebook
}

export enum AuthActionEnum {
  Login,
  Register
}
