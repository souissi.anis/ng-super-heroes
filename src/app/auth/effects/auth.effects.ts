import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import {
  tap,
  map,
  exhaustMap,
  catchError,
  takeLast,
  take,
  switchMap
} from 'rxjs/operators';

import * as fromAuth from '../reducers';
import { AuthService } from '../services/auth.service';
import {
  InitUser,
  LoginWithEmail,
  LoginWithProvider,
  Logout,
  LoginSuccess,
  LoginFailure,
  AuthActionTypes
} from '../actions/auth';
import { Load, Clear } from '../../core/actions/favorite';
import { User, Authenticate, AuthProviderEnum } from '../models/auth';
import { Store, select } from '@ngrx/store';

@Injectable()
export class AuthEffects {
  @Effect()
  initUser$ = this.actions$.pipe(
    ofType(AuthActionTypes.InitUser),
    map((action: InitUser) => action),
    exhaustMap(auth =>
      this.authService.getUser().pipe(
        take(1),
        map(user => {
          if (user) {
            return new LoginSuccess({ user: user, withRedirect: false });
          } else {
            return new LoginFailure(null);
          }
        }),
        catchError(error => of(new LoginFailure(error)))
      )
    )
  );

  @Effect()
  loginWithEmail$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginWithEmail),
    map((action: LoginWithEmail) => action.payload),
    exhaustMap((auth: Authenticate) =>
      this.authService
        .loginWithEmail(auth)
        .pipe(
          map(user => new LoginSuccess({ user, withRedirect: true })),
          catchError(error => of(new LoginFailure(error)))
        )
    )
  );

  @Effect()
  loginWithProvider$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginWithProvider),
    map((action: LoginWithProvider) => action.payload),
    exhaustMap((authProvider: AuthProviderEnum) =>
      this.authService
        .loginWithProvider(authProvider)
        .pipe(
          map(user => new LoginSuccess({ user, withRedirect: true })),
          catchError(error => of(new LoginFailure(error)))
        )
    )
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    map((action: LoginSuccess) => action.payload),
    tap((authed: { user: User; withRedirect: boolean }) => {
      if (authed.withRedirect) {
        const returnUrlSubscription = this.store
          .pipe(select(fromAuth.getReturnUrl))
          .subscribe(returnUrl => this.router.navigate([returnUrl]));
        returnUrlSubscription.unsubscribe();
      }
    }),
    switchMap(authed => of(new Load(authed.user.uid)))
  );

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect),
    tap(authed => {
      this.router.navigate(['/login']);
    })
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    exhaustMap(() => this.authService.logout()),
    switchMap(() => {
      if (this.router.url === '/heroes/favorite') {
        this.router.navigate(['/']);
      }
      return of(new Clear());
    })
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store<fromAuth.State>
  ) {}
}
