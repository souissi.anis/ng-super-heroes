import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromAuth from '../reducers';
import * as Auth from '../actions/auth';
import { Authenticate, AuthProviderEnum } from '../models/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  pending$ = this.store.pipe(select(fromAuth.getLoginPagePending));
  error$ = this.store.pipe(select(fromAuth.getLoginPageError));
  user$ = this.store.pipe(select(fromAuth.getUser));

  constructor(private store: Store<fromAuth.State>) { }

  ngOnInit() {
  }

  onSubmit($event: Authenticate) {
    this.store.dispatch(new Auth.LoginWithEmail($event));
  }

  onSignInWith($event: AuthProviderEnum) {
    this.store.dispatch(new Auth.LoginWithProvider($event));
  }

  logout() {
    this.store.dispatch(new Auth.Logout());
  }

}
