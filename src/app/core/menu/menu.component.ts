import { Component, OnInit, Output, EventEmitter, Input  } from '@angular/core';
import { Hero } from '../../heroes/models/hero';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Output() closeMenu = new EventEmitter();
  @Input() favoriteHeroes: Hero[];
  @Input() loggedIn = false;
  @Input() isLoadingFavorite = false;

  get topFiveHeroes() {
    if (this.favoriteHeroes && this.favoriteHeroes.length > 0) {
      return this.favoriteHeroes.slice(0, 5);
    }
    return null;
  }

  constructor() { }

  ngOnInit() {
  }

}
