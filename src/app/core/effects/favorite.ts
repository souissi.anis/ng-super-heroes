import { Injectable } from '@angular/core';
import { Action, Store, select } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import * as fromFavorite from '../../reducers';

import {
  FavoriteActions,
  FavoriteActionTypes,
  LoadFail,
  LoadSuccess,
  AddHero,
  AddHeroSuccess,
  AddHeroFail,
  RemoveHero,
  RemoveHeroFail,
  RemoveHeroSuccess,
  Load,
  Init
} from './../actions/favorite';
import { Hero } from '../../heroes/models/hero';
import {
  switchMap,
  toArray,
  map,
  catchError,
  mergeMap,
  exhaustMap,
  take
} from 'rxjs/operators';
import { FavoriteService } from '../services/favorite.service';
import { Favorite } from '../models/favorite';
import { merge } from 'rxjs/operators/merge';

@Injectable()
export class FavoriteEffects {
  @Effect()
  loadFavorite$: Observable<Action> = this.actions$.pipe(
    ofType(FavoriteActionTypes.Load),
    map((action: Load) => action.payload),
    switchMap(uid =>
      this.favoriteService
        .getCollection(ref => ref.where('uid', '==', uid))
        .pipe(
          map(favorite => {
            // initialize a new collection for the user
            if (!favorite || favorite.length === 0) {
              return new Init(uid);
            } else {
              return new LoadSuccess(favorite[0]);
            }
          }),
          catchError(error => of(new LoadFail(error)))
        )
    )
  );

  @Effect()
  initFavorite$: Observable<Action> = this.actions$.pipe(
    ofType(FavoriteActionTypes.Init),
    map((action: Init) => action.payload),
    switchMap(uid => {
      const favoriteToAdd = { uid: uid, heroes: [], created: new Date() };
      return this.favoriteService
          .add(favoriteToAdd)
          .map(added => new Load(uid));
    })
  );

  @Effect()
  addHeroToFavorite$: Observable<Action> = this.actions$.pipe(
    ofType(FavoriteActionTypes.AddHero),
    map((action: AddHero) => action.payload),
    switchMap((heroToAdd: Hero) => {
      return this.store
        .pipe(
          select(fromFavorite.getFavorite),
          take(1),
          map(favorite =>
            this.favoriteService.update(favorite.id, {
              heroes: [...favorite.heroes, heroToAdd]
            })
          )
        )
        .pipe(
          switchMap(added => added.map(() => new AddHeroSuccess(heroToAdd))),
          catchError(error => of(new AddHeroFail(error)))
        );
    })
  );

  @Effect()
  removeHeroFromFavorite$: Observable<Action> = this.actions$.pipe(
    ofType(FavoriteActionTypes.RemoveHero),
    map((action: RemoveHero) => action.payload),
    switchMap((heroToRemove: Hero) => {
      return this.store
        .pipe(
          select(fromFavorite.getFavorite),
          take(1),
          map(favorite =>
            this.favoriteService.update(favorite.id, {
              heroes: favorite.heroes.filter(
                hero => hero.id !== heroToRemove.id
              )
            })
          )
        )
        .pipe(
          switchMap(removed =>
            removed.map(() => new RemoveHeroSuccess(heroToRemove))
          ),
          catchError(error => of(new RemoveHeroFail(error)))
        );
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<fromFavorite.State>,
    private favoriteService: FavoriteService
  ) {}
}
