import { Hero } from '../../heroes/models/hero';

export interface Favorite {
  uid: string;
  heroes: Hero[];
  created?: Date;
  id?: string;
}
