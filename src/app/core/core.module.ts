import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material/material.module';
import { LayoutComponent } from './layout/layout.component';
import { MenuComponent } from './menu/menu.component';
import { FavoriteTopFiveComponent } from './favorite-top-five/favorite-top-five.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { HeroesService } from './services/heroes.service';
import { FavoriteService } from './services/favorite.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  exports: [
    LayoutComponent
  ],
  declarations: [LayoutComponent, MenuComponent, FavoriteTopFiveComponent, WelcomeComponent, PageNotFoundComponent]
})
export class CoreModule {
  static forRoot() {
    return {
      ngModule: CoreModule,
      providers: [HeroesService, FavoriteService]
    };
  }
}
