import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../../heroes/models/hero';

@Component({
  selector: 'app-favorite-top-five',
  templateUrl: './favorite-top-five.component.html',
  styleUrls: ['./favorite-top-five.component.css']
})
export class FavoriteTopFiveComponent implements OnInit {
  @Input() heroes: Hero[];
  @Input() loggedIn = false;

  getThumbnail(hero: Hero) {
      return hero.thumbnail.path + '.' + hero.thumbnail.extension;
  }

  constructor() { }

  ngOnInit() {
  }

}
