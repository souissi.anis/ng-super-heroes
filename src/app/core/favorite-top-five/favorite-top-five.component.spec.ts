import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteTopFiveComponent } from './favorite-top-five.component';

describe('FavoriteTopFiveComponent', () => {
  let component: FavoriteTopFiveComponent;
  let fixture: ComponentFixture<FavoriteTopFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteTopFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteTopFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
