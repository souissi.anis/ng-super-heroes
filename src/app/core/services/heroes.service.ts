import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MD5 } from 'crypto-js';
import { Hero, HeroSearchResult } from '../../heroes/models/hero';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HeroesService {
  private API_PUBLIC_KEY = environment.marvelApiConfig.apiPublicKey;
  private API_PRIVATE_KEY = environment.marvelApiConfig.apiPrivateKey;
  private API_PATH = environment.marvelApiConfig.apiPath;

  constructor(private http: HttpClient) {}

  search(term: string, offset: number, limit: number): Observable<HeroSearchResult> {
    const ts = new Date().getTime();
    const hash = MD5(ts + this.API_PRIVATE_KEY + this.API_PUBLIC_KEY);
    const url = `${this.API_PATH}?nameStartsWith=${term}&ts=${ts}&apikey=${this.API_PUBLIC_KEY}
&hash=${hash}&limit=${limit}&offset=${(offset * limit)}`;
    return this.http
               .get(url)
               .pipe(map(result => this.parseResult(result)));
  }

  retrieveHero(id: number): Observable<Hero> {
    const ts = new Date().getTime();
    const hash = MD5(ts + this.API_PRIVATE_KEY + this.API_PUBLIC_KEY);
    return this.http
               .get(`${this.API_PATH}/${id}?ts=${ts}&apikey=${this.API_PUBLIC_KEY}&hash=${hash}`)
               .pipe(map(result => this.initializeHero((result as any).data.results[0])));
  }

  private parseResult(result: any): HeroSearchResult {
    return {
      heroes: result.data.results.map(r => {
        return this.initializeHero(r);
      }),
      count: result.data.count,
      limit: result.data.limit,
      offset: result.data.offset,
      total: result.data.total
    };
  }

  private initializeHero = (r): Hero => ({
      id: r.id,
      name: r.name,
      description: r.description,
      thumbnail: {
        path: r.thumbnail.path && r.thumbnail.path.length > 0 ? r.thumbnail.path.replace('http', 'https') : '',
        extension: r.thumbnail.extension
      }
  })
}
