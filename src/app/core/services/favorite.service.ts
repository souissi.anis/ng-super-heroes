import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Favorite } from '../models/favorite';
import * as firebase from 'firebase/app';
import DocumentReference = firebase.firestore.DocumentReference;
import { QueryFn } from 'angularfire2/firestore/interfaces';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FavoriteService {
  readonly path = 'favorite';

  constructor(private afs: AngularFirestore) {}

  add(data: Favorite): Observable<DocumentReference> {
    return Observable.fromPromise(
      this.afs
        .collection<Favorite>(this.path)
        .add({ ...data, created: new Date() })
    );
  }

  remove(id: string): Observable<void> {
    return Observable.fromPromise(
      this.afs.doc<Favorite>(`${this.path}/${id}`).delete()
    );
  }

  update(id: string, data: Partial<Favorite>): Observable<void> {
    return Observable.fromPromise(
      this.afs.doc<Favorite>(`${this.path}/${id}`).update(data)
    );
  }

  getCollection(ref?: QueryFn): Observable<Favorite[]> {
    return this.afs
      .collection<Favorite>(this.path, ref)
      .snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Favorite;
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }
}
