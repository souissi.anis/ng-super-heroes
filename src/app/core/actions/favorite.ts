import { Action } from '@ngrx/store';
import { Hero } from '../../heroes/models/hero';
import { Favorite } from '../models/favorite';

export enum FavoriteActionTypes {
  Load = '[Favorite] Load',
  Init = '[Favorite] Initialize new favorite',
  LoadSuccess = '[Favorite] Load Success',
  LoadFail = '[Favorite] Load Fail',
  Clear = '[Favorite] Clear',
  AddHero = '[Favorite] Add Hero',
  AddHeroSuccess = '[Favorite] Add Hero Success',
  AddHeroFail = '[Favorite] Add Hero Fail',
  RemoveHero = '[Favorite] Remove Hero',
  RemoveHeroSuccess = '[Favorite] Remove Hero Success',
  RemoveHeroFail = '[Favorite] Remove Hero Fail'
}

/**
 * Load Favorite Actions
 */
export class Load implements Action {
  readonly type = FavoriteActionTypes.Load;

  constructor(public payload: string) {}
}

export class Init implements Action {
  readonly type = FavoriteActionTypes.Init;

  constructor(public payload: string) {}
}

export class LoadSuccess implements Action {
  readonly type = FavoriteActionTypes.LoadSuccess;

  constructor(public payload: Favorite) {}
}

export class LoadFail implements Action {
  readonly type = FavoriteActionTypes.LoadFail;

  constructor(public payload: any) {}
}

export class Clear implements Action {
  readonly type = FavoriteActionTypes.Clear;
}

/**
 * Add Hero to Favorite Actions
 */
export class AddHero implements Action {
  readonly type = FavoriteActionTypes.AddHero;

  constructor(public payload: Hero) {}
}

export class AddHeroSuccess implements Action {
  readonly type = FavoriteActionTypes.AddHeroSuccess;

  constructor(public payload: Hero) {}
}

export class AddHeroFail implements Action {
  readonly type = FavoriteActionTypes.AddHeroFail;

  constructor(public payload: string) {}
}

/**
 * Remove Hero from Favorite Actions
 */
export class RemoveHero implements Action {
  readonly type = FavoriteActionTypes.RemoveHero;

  constructor(public payload: Hero) {}
}

export class RemoveHeroSuccess implements Action {
  readonly type = FavoriteActionTypes.RemoveHeroSuccess;

  constructor(public payload: Hero) {}
}

export class RemoveHeroFail implements Action {
  readonly type = FavoriteActionTypes.RemoveHeroFail;

  constructor(public payload: string) {}
}

export type FavoriteActions =
  | Load
  | LoadSuccess
  | LoadFail
  | Clear
  | AddHero
  | AddHeroSuccess
  | AddHeroFail
  | RemoveHero
  | RemoveHeroSuccess
  | RemoveHeroFail;
