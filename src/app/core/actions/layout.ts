import { Action } from '@ngrx/store';

export enum LayoutActionTypes {
  SetIsMobile = '[Layout] Set IsMobile'
}

export class SetIsMobile implements Action {
  readonly type = LayoutActionTypes.SetIsMobile;

  constructor(public payload: boolean) {}
}

export type LayoutActions = SetIsMobile;
