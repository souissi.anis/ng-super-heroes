import { LayoutActionTypes, LayoutActions } from '../actions/layout';

export interface State {
  isMobile: boolean;
}

const initialState: State = {
    isMobile: false,
};

export function reducer(
  state: State = initialState,
  action: LayoutActions
): State {
  switch (action.type) {
    case LayoutActionTypes.SetIsMobile:
      return {
        isMobile: action.payload,
      };

    default:
      return state;
  }
}

export const getIsMobile = (state: State) => state.isMobile;
