import { FavoriteActionTypes, FavoriteActions } from './../actions/favorite';
import { Hero } from '../../heroes/models/hero';
import { Favorite } from '../models/favorite';

export interface State {
  loaded: boolean;
  loading: boolean;
  favorite: Favorite;
  error: string | null;
}

const initialState: State = {
  loaded: false,
  loading: false,
  favorite: { heroes: [], uid: '' },
  error: null
};

export function reducer(state = initialState, action: FavoriteActions): State {
  switch (action.type) {
    case FavoriteActionTypes.Load: {
      return {
        ...state,
        loading: true,
        favorite: { ...state.favorite, uid: action.payload }
      };
    }

    case FavoriteActionTypes.LoadSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        favorite: {
          ...state.favorite,
          heroes: action.payload.heroes,
          created: action.payload.created,
          id: action.payload.id
        }
      };
    }

    case FavoriteActionTypes.AddHeroSuccess: {
      if (
        state.favorite.heroes.map(h => h.id).indexOf(action.payload.id) > -1
      ) {
        return state;
      }

      return {
        ...state,
        favorite: {
          ...state.favorite,
          heroes: [...state.favorite.heroes, action.payload]
        }
      };
    }

    case FavoriteActionTypes.RemoveHeroFail: {
      return {
        ...state,
        error: action.payload
      };
    }

    case FavoriteActionTypes.RemoveHeroSuccess: {
      return {
        ...state,
        favorite: {
          ...state.favorite,
          heroes: state.favorite.heroes.filter(h => h.id !== action.payload.id)
        }
      };
    }

    case FavoriteActionTypes.AddHeroFail: {
      return {
        ...state,
        error: action.payload
      };
    }

    case FavoriteActionTypes.Clear: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getFavorite = (state: State) => state.favorite;

export const getHeroes = (state: State) => state.favorite.heroes;

export const getIds = (state: State) => state.favorite.heroes.map(h => h.id);

export const getUid = (state: State) => state.favorite.uid;
