import { Component, ChangeDetectorRef, OnDestroy, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import * as fromFavorite from '../../reducers';
import * as fromAuth from '../../auth/reducers';
import { Hero } from '../../heroes/models/hero';
import { Observable } from 'rxjs/Observable';
import { MatSidenav } from '@angular/material';
import { User } from '../../auth/models/auth';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  @Input() isMobile = false;
  @Input() loggedIn = false;
  @Output() logOut = new EventEmitter();

  @ViewChild(MatSidenav)
  private sideNav: MatSidenav;

  year = (new Date()).getFullYear();

  favoriteHeroes$: Observable<Hero[]>;
  isLoadingFavorite$: Observable<boolean>;
  user$: Observable<User>;

  constructor(private router: Router, private store: Store<fromFavorite.State>) {
    this.favoriteHeroes$ = this.store.pipe(select(fromFavorite.getHeroes));
    this.isLoadingFavorite$ = this.store.pipe(select(fromFavorite.getFavoriteLoading));
    this.user$ = this.store.pipe(select(fromAuth.getUser));
  }

  navigateTo(url: string) {
    if (this.isMobile) {
      this.sideNav.close();
    }
    this.router.navigateByUrl(url);
  }

  logout() {
    if (this.isMobile) {
      this.sideNav.close();
    }
    this.logOut.emit();
  }

}
