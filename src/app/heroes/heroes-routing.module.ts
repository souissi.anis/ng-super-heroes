import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { HeroSelectedComponent } from './hero-selected/hero-selected.component';
import { HeroExistsGuard } from './guards/hero-exists';
import { HeroFavoriteComponent } from './hero-favorite/hero-favorite.component';
import { AuthGuardService } from '../auth/services/auth-guard.service';

const routes: Routes = [
   { path: 'search', component: HeroSearchComponent },
   {
     path: 'favorite',
     component: HeroFavoriteComponent,
     canActivate: [AuthGuardService]
   },
   {
     path: ':id',
     component: HeroSelectedComponent,
     canActivate: [HeroExistsGuard]
   },
   { path: '', component: HeroSearchComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule { }
