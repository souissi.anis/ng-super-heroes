import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../models/hero';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as favorite from '../../core/actions/favorite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-hero-card-list',
  templateUrl: './hero-card-list.component.html',
  styleUrls: ['./hero-card-list.component.css']
})
export class HeroCardListComponent implements OnInit {
  @Input() heroes: Hero[];
  @Input() loggedIn: boolean;

  constructor(private store: Store<fromRoot.State>) {}

  ngOnInit() {}

  inFavorite(heroId: number): Observable<boolean> {
    return this.store
      .pipe(select(fromRoot.getFavoriteHeroIds))
      .pipe(map(ids => ids.indexOf(heroId) > -1));
  }

  addToFavorites(hero: Hero) {
    this.store.dispatch(new favorite.AddHero(hero));
  }

  removeFromFavorites(hero: Hero) {
    this.store.dispatch(new favorite.RemoveHero(hero));
  }
}
