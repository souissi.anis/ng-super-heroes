import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '../models/hero';

@Component({
  selector: 'app-hero-card',
  templateUrl: './hero-card.component.html',
  styleUrls: ['./hero-card.component.css']
})
export class HeroCardComponent implements OnInit {
  @Input() hero: Hero;
  @Input() inFavorite: boolean;
  @Input() loggedIn: boolean;
  @Output() add = new EventEmitter<number>();
  @Output() remove = new EventEmitter<number>();
  @Output() details = new EventEmitter<number>();

  get thumbnail() {
    return this.hero.thumbnail.path + '.' + this.hero.thumbnail.extension;
  }

  constructor() {}

  ngOnInit() {}
}
