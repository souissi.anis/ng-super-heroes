import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operators';

import * as fromHeroes from '../reducers';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import * as hero from '../actions/hero';
import { Hero, HeroSearchPager } from '../models/hero';

@Component({
  selector: 'app-hero-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {
  @ViewChild('searchContainer') private searchContainer: ElementRef;

  isMobile$: Observable<boolean>;
  loggedIn$: Observable<boolean>;
  searchQuery$: Observable<string>;
  heroes$: Observable<Hero[]>;
  loading$: Observable<boolean>;
  error$: Observable<string>;
  count$: Observable<number>;
  limit$: Observable<number>;
  offset$: Observable<number>;
  total$: Observable<number>;

  constructor(private store: Store<fromHeroes.State>) {
    this.isMobile$ = this.store.pipe(select(fromRoot.getIsMobile));
    this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
    this.searchQuery$ = store.pipe(select(fromHeroes.getSearchQuery));
    this.heroes$ = store.pipe(select(fromHeroes.getSearchResults));
    this.loading$ = store.pipe(select(fromHeroes.getSearchLoading));
    this.error$ = store.pipe(select(fromHeroes.getSearchError));
    this.count$ = store.pipe(select(fromHeroes.getSearchCount));
    this.limit$ = store.pipe(select(fromHeroes.getSearchLimit));
    this.offset$ = store.pipe(select(fromHeroes.getSearchOffset));
    this.total$ = store.pipe(select(fromHeroes.getSearchTotal));
  }

  ngOnInit() {}

  search(query: string) {
    this.store.dispatch(new hero.Search(query));
  }

  paginate(event: HeroSearchPager) {
    this.store.dispatch(new hero.Search(event));

    // experimental scroll
    try {
      this.searchContainer.nativeElement.parentElement.parentElement.scrollTop = 0;
    } catch (error) {
      console.log(error);
    }
  }
}
