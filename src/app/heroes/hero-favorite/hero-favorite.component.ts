import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Hero } from '../models/hero';
import * as fromFavorite from '../../reducers';
import * as fromRoot from '../../reducers';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-hero-favorite',
  templateUrl: './hero-favorite.component.html',
  styleUrls: ['./hero-favorite.component.css']
})
export class HeroFavoriteComponent implements OnInit {
  favoriteHeroes$: Observable<Hero[]>;
  isMobile$: Observable<boolean>;

  constructor(store: Store<fromFavorite.State>) {
    this.favoriteHeroes$ = store.pipe(select(fromFavorite.getHeroes));
    this.isMobile$ = store.pipe(select(fromRoot.getIsMobile));
  }

  ngOnInit() {}
}
