import { Action } from '@ngrx/store';
import { Hero, HeroSearchResult, HeroSearchPager } from '../models/hero';

export enum HeroActionTypes {
  Search = '[Hero] Search',
  SearchComplete = '[Hero] Search Complete',
  SearchError = '[Hero] Search Error',
  Load = '[Hero] Load',
  Select = '[Hero] Select',
}

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class Search implements Action {
  readonly type = HeroActionTypes.Search;

  constructor(public payload: string | HeroSearchPager) {}
}

export class SearchComplete implements Action {
  readonly type = HeroActionTypes.SearchComplete;

  constructor(public payload: HeroSearchResult) {}
}

export class SearchError implements Action {
  readonly type = HeroActionTypes.SearchError;

  constructor(public payload: string) {}
}

export class Load implements Action {
  readonly type = HeroActionTypes.Load;

  constructor(public payload: Hero) {}
}

export class Select implements Action {
  readonly type = HeroActionTypes.Select;

  constructor(public payload: number) {}
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type HeroActions = Search | SearchComplete | SearchError | Load | Select;
