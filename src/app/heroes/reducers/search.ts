import { HeroActionTypes, HeroActions } from '../actions/hero';
import { HeroSearchPager } from '../models/hero';

export interface State {
  ids: number[];
  loading: boolean;
  error: string;
  query: string;
  count: number;
  limit: number;
  offset: number;
  total: number;
}

const initialState: State = {
  ids: [],
  loading: false,
  error: '',
  query: '',
  count: 0,
  limit: 20,
  offset: 0,
  total: 0
};

export function reducer(state = initialState, action: HeroActions): State {
  switch (action.type) {
    case HeroActionTypes.Search: {
      const searchCriteria = action.payload;

      let query = '';
      let offset = 0;
      let limit = 20;

      if (typeof searchCriteria === 'string') {
        query = searchCriteria;
      } else {
        query = (searchCriteria as HeroSearchPager).query;
        offset = (searchCriteria as HeroSearchPager).offset;
        limit = (searchCriteria as HeroSearchPager).limit;
      }

      if (query === '') {
        return {
          ids: [],
          loading: false,
          error: '',
          query,
          count: 0,
          limit: 20,
          offset: 0,
          total: 0
        };
      }

      return {
        ...state,
        loading: true,
        error: '',
        query,
        limit,
        offset
      };
    }

    case HeroActionTypes.SearchComplete: {
      return {
        ids: action.payload.heroes.map(hero => hero.id),
        loading: false,
        error: '',
        query: state.query,
        count: action.payload.count,
        limit: action.payload.limit,
        offset: action.payload.offset,
        total: action.payload.total
      };
    }

    case HeroActionTypes.SearchError: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;

export const getCount = (state: State) => state.count;

export const getLimit = (state: State) => state.limit;

export const getOffset = (state: State) => state.offset;

export const getTotal = (state: State) => state.total;
