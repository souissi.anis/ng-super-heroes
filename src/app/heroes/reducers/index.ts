import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromSearch from './search';
import * as fromHeroes from './heroes';
import * as fromRoot from '../../reducers';

export interface HeroesState {
  search: fromSearch.State;
  heroes: fromHeroes.State;
}

export interface State extends fromRoot.State {
  heroes: HeroesState;
}

export const reducers = {
  search: fromSearch.reducer,
  heroes: fromHeroes.reducer
};

/**
 * A selector function is a map function factory. We pass it parameters and it
 * returns a function that maps from the larger state tree into a smaller
 * piece of state. This selector simply selects the `heroes` state.
 *
 * Selectors are used with the `select` operator.
 *
 * ```ts
 * class MyComponent {
 *   constructor(state$: Observable<State>) {
 *     this.heroesState$ = state$.pipe(select(getHeroesState));
 *   }
 * }
 * ```
 */

/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
 */
export const getHeroesState = createFeatureSelector<HeroesState>('heroes');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them usable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */
export const getHeroEntitiesState = createSelector(
  getHeroesState,
  state => state.heroes
);

export const getSelectedHeroId = createSelector(
  getHeroEntitiesState,
  fromHeroes.getSelectedId
);

/**
 * Adapters created with @ngrx/entity generate
 * commonly used selector functions including
 * getting all ids in the record set, a dictionary
 * of the records by id, an array of records and
 * the total number of records. This reduces boilerplate
 * in selecting records from the entity state.
 */
export const {
  selectIds: getHeroIds,
  selectEntities: getHeroEntities,
  selectAll: getAllHeroes,
  selectTotal: getTotalHeroes
} = fromHeroes.adapter.getSelectors(getHeroEntitiesState);

export const getSelectedHero = createSelector(
  getHeroEntities,
  getSelectedHeroId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

/**
 * Just like with the heroes selectors, we also have to compose the search
 * reducer's and Favorite reducer's selectors.
 */
export const getSearchState = createSelector(
  getHeroesState,
  (state: HeroesState) => state.search
);

export const getSearchHeroIds = createSelector(
  getSearchState,
  fromSearch.getIds
);
export const getSearchQuery = createSelector(
  getSearchState,
  fromSearch.getQuery
);
export const getSearchLoading = createSelector(
  getSearchState,
  fromSearch.getLoading
);
export const getSearchError = createSelector(
  getSearchState,
  fromSearch.getError
);
export const getSearchCount = createSelector(
  getSearchState,
  fromSearch.getCount
);
export const getSearchLimit = createSelector(
  getSearchState,
  fromSearch.getLimit
);
export const getSearchOffset = createSelector(
  getSearchState,
  fromSearch.getOffset
);
export const getSearchTotal = createSelector(
  getSearchState,
  fromSearch.getTotal
);

/**
 * Some selector functions create joins across parts of state. This selector
 * composes the search result IDs to return an array of heroes in the store.
 */
export const getSearchResults = createSelector(
  getHeroEntities,
  getSearchHeroIds,
  (heroes, searchIds) => {
    return searchIds.map(id => heroes[id]);
  }
);

export const isSelectedHeroInFavorite = createSelector(
  fromRoot.getFavoriteHeroIds,
  getSelectedHeroId,
  (ids, selected) => {
    // fix for selected passed as string
    const selectedAsNumber = Number.parseInt(selected.toString());
    return ids.indexOf(selectedAsNumber) > -1;
  }
);
