import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../models/hero';

@Component({
  selector: 'app-hero-search-result',
  templateUrl: './hero-search-result.component.html',
  styleUrls: ['./hero-search-result.component.css']
})
export class HeroSearchResultComponent implements OnInit {
  @Input() result: Hero[];
  @Input() searching = false;
  @Input() loggedIn = false;

  constructor() { }

  ngOnInit() {
  }

}
