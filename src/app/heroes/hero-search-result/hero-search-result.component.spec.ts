import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSearchResultComponent } from './hero-search-result.component';

describe('HeroSearchResultComponent', () => {
  let component: HeroSearchResultComponent;
  let fixture: ComponentFixture<HeroSearchResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroSearchResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
