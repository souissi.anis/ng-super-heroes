import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { filter, take, map, tap, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { HeroesService } from '../../core/services/heroes.service';
import * as fromHeroes from '../reducers';
import * as fromFavorite from '../../reducers';
import * as fromAuth from '../../auth/reducers';
import * as hero from '../actions/hero';

/**
 * Guards are hooks into the route resolution process, providing an opportunity
 * to inform the router's navigation process whether the route should continue
 * to activate this route. Guards must return an observable of true or false.
 */
@Injectable()
export class HeroExistsGuard implements CanActivate {
  constructor(
    private store: Store<fromFavorite.State>,
    private heroesService: HeroesService,
    private router: Router
  ) {
    this.loggedIn$ = store.pipe(select(fromAuth.getLoggedIn));
  }

  loggedIn$: Observable<boolean>;

  /**
   * This method creates an observable that waits for the `loaded` property
   * of the favorite state to turn `true`, emitting one time once loading
   * has finished.
   */
  waitForFavoriteToLoad(): Observable<boolean> {
    return this.store.pipe(
      select(fromFavorite.getFavoriteLoaded),
      filter(loaded => loaded),
      take(1)
    );
  }

  /**
   * This method checks if a hero with the given ID is already registered
   * in the Store
   */
  hasHeroInStore(id: number): Observable<boolean> {
    return this.store.pipe(
      select(fromHeroes.getHeroEntities),
      map(entities => !!entities[id]),
      take(1)
    );
  }

  /**
   * This method loads a hero with the given ID from the API and caches
   * it in the store, returning `true` or `false` if it was found.
   */
  hasHeroInApi(id: number): Observable<boolean> {
    return this.heroesService.retrieveHero(id).pipe(
      map(heroEntity => new hero.Load(heroEntity)),
      tap((action: hero.Load) => this.store.dispatch(action)),
      map(heroInApi => !!heroInApi),
      catchError(() => {
        this.router.navigate(['/404']);
        return of(false);
      })
    );
  }

  /**
   * `hasHero` composes `hasHeroInStore` and `hasHeroInApi`. It first checks
   * if the hero is in store, and if not it then checks if it is in the
   * API.
   */
  hasHero(id: number): Observable<boolean> {
    return this.hasHeroInStore(id).pipe(
      switchMap(inStore => {
        if (inStore) {
          return of(inStore);
        }

        return this.hasHeroInApi(id);
      })
    );
  }

  /**
   * This is the actual method the router will call when our guard is run.
   *
   * Our guard waits for the favorite to load, then it checks if we need
   * to request a hero from the API or if we already have it in our cache.
   * If it finds it in the cache or in the API, it returns an Observable
   * of `true` and the route is rendered successfully.
   *
   * If it was unable to find it in our cache or in the API, this guard
   * will return an Observable of `false`, causing the router to move
   * on to the next candidate route. In this case, it will move on
   * to the 404 page.
   */
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.loggedIn$.switchMap(authed => {
      if (!authed) {
        return this.hasHero(route.params['id']);
      }
      return this.waitForFavoriteToLoad().pipe(
        switchMap(() => this.hasHero(route.params['id']))
      );
    });
  }
}
