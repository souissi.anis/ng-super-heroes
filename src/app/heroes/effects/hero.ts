import { Injectable, InjectionToken, Optional, Inject } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Scheduler } from 'rxjs/Scheduler';
import { async } from 'rxjs/scheduler/async';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

import { HeroesService } from '../../core/services/heroes.service';
import { HeroSearchPager } from '../models/hero';
import {
  HeroActionTypes,
  HeroActions,
  SearchComplete,
  SearchError,
  Search,
} from '../actions/hero';
import { HeroSearchResult } from '../models/hero';
import {
  debounceTime,
  map,
  switchMap,
  skip,
  takeUntil,
  catchError,
} from 'rxjs/operators';
import { query } from '@angular/core/src/animation/dsl';

export const SEARCH_DEBOUNCE = new InjectionToken<number>('Search Debounce');
export const SEARCH_SCHEDULER = new InjectionToken<Scheduler>(
  'Search Scheduler'
);

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */

@Injectable()
export class HeroEffects {
  @Effect()
  search$: Observable<Action> = this.actions$.pipe(
    ofType<Search>(HeroActionTypes.Search),
    debounceTime(this.debounce || 300, this.scheduler || async),
    map(action => action.payload),
    switchMap(searchCriteria => {
      let term = '';
      let offset = 0;
      let limit = 20;

      if (typeof searchCriteria === 'string') {
        term = searchCriteria;
      } else {
        term = (searchCriteria as HeroSearchPager).query;
        offset = (searchCriteria as HeroSearchPager).offset;
        limit = (searchCriteria as HeroSearchPager).limit;
      }

      if (term  === '') {
        return empty();
      }

      const nextSearch$ = this.actions$.pipe(
        ofType(HeroActionTypes.Search),
        skip(1)
      );

      return this.heroesService
        .search(term, offset, limit)
        .pipe(
          takeUntil(nextSearch$),
          map((heroSearchResult: HeroSearchResult) => new SearchComplete(heroSearchResult)),
          catchError(err => of(new SearchError(err)))
        );
    })
  );

  constructor(
    private actions$: Actions,
    private heroesService: HeroesService,
    @Optional()
    @Inject(SEARCH_DEBOUNCE)
    private debounce: number,
    /**
     * You inject an optional Scheduler that will be undefined
     * in normal application usage, but its injected here so that you can mock out
     * during testing using the RxJS TestScheduler for simulating passages of time.
     */
    @Optional()
    @Inject(SEARCH_SCHEDULER)
    private scheduler: Scheduler
  ) {}
}
