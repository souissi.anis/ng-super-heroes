import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material';
import { HeroSearchPager } from '../models/hero';

@Component({
  selector: 'app-hero-search-result-pager',
  templateUrl: './hero-search-result-pager.component.html',
  styleUrls: ['./hero-search-result-pager.component.css']
})
export class HeroSearchResultPagerComponent implements OnInit {
  @Input() query = '';
  @Input() length = 0;
  @Input() pageSize = 20;
  @Input() pageSizeOptions = [5, 10, 20, 100];
  @Output() paginate = new EventEmitter<HeroSearchPager>();

  constructor() { }

  ngOnInit() {
  }

  pageEvent(event: PageEvent) {
    this.paginate.emit({
      query: this.query,
      limit: event.pageSize,
      offset: event.pageIndex
    });
  }

}
