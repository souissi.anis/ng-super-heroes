import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSearchResultPagerComponent } from './hero-search-result-pager.component';

describe('HeroSearchResultPagerComponent', () => {
  let component: HeroSearchResultPagerComponent;
  let fixture: ComponentFixture<HeroSearchResultPagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroSearchResultPagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSearchResultPagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
