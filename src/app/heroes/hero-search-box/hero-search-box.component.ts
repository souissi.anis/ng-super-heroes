import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hero-search-box',
  templateUrl: './hero-search-box.component.html',
  styleUrls: ['./hero-search-box.component.css']
})
export class HeroSearchBoxComponent implements OnInit {
  @Input() query = '';
  @Input() searching = false;
  @Input() error = '';
  @Output() search = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

}
