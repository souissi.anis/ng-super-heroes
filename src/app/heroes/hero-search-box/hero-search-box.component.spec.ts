import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSearchBoxComponent } from './hero-search-box.component';

describe('HeroSearchBoxComponent', () => {
  let component: HeroSearchBoxComponent;
  let fixture: ComponentFixture<HeroSearchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroSearchBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
