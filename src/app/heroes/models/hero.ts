export interface Hero {
    id: number;
    name: string;
    description: string;
    thumbnail: {
        path: string;
        extension: string;
    };
}

export interface HeroSearchResult {
    heroes: Hero[];
    count: number;
    limit: number;
    offset: number;
    total: number;
}

export interface HeroSearchPager {
    query: string;
    limit: number;
    offset: number;
}
