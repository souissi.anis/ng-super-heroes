import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { HeroesRoutingModule } from './heroes-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { HeroSearchComponent } from './hero-search/hero-search.component';
import { HeroSearchBoxComponent } from './hero-search-box/hero-search-box.component';
import { HeroSearchResultComponent } from './hero-search-result/hero-search-result.component';
import { HeroCardComponent } from './hero-card/hero-card.component';
import { HeroCardListComponent } from './hero-card-list/hero-card-list.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';

import { reducers } from './reducers';
import { HeroEffects } from './effects/hero';
import { HeroSearchResultPagerComponent } from './hero-search-result-pager/hero-search-result-pager.component';
import { HeroSelectedComponent } from './hero-selected/hero-selected.component';
import { HeroExistsGuard } from './guards/hero-exists';
import { HeroFavoriteComponent } from './hero-favorite/hero-favorite.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HeroesRoutingModule,

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('heroes', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([HeroEffects])
  ],
  declarations: [
    HeroSearchComponent,
    HeroSearchBoxComponent,
    HeroSearchResultComponent,
    HeroCardComponent,
    HeroCardListComponent,
    HeroDetailComponent,
    HeroSearchResultPagerComponent,
    HeroSelectedComponent,
    HeroFavoriteComponent
  ],
  providers: [HeroExistsGuard]
})
export class HeroesModule {}
