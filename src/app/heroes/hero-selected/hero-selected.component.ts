import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import * as fromHeroes from '../reducers';
import * as fromRoot from '../../reducers';
import * as fromAuth from '../../auth/reducers';
import * as hero from '../actions/hero';
import * as favorite from '../../core/actions/favorite';

import { Hero } from '../models/hero';

@Component({
  selector: 'app-hero-selected',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './hero-selected.component.html',
  styleUrls: ['./hero-selected.component.css']
})
export class HeroSelectedComponent implements OnDestroy {
  actionsSubscription: Subscription;
  hero$: Observable<Hero>;
  isMobile$: Observable<boolean>;
  inFavorite$: Observable<boolean>;
  loggedIn$: Observable<boolean>;

  constructor(private store: Store<fromHeroes.State>, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .pipe(map(params => new hero.Select(params.id)))
      .subscribe(store);

    this.hero$ = store.pipe(select(fromHeroes.getSelectedHero));
    this.isMobile$ = store.pipe(select(fromRoot.getIsMobile));
    this.inFavorite$ = store.pipe(select(fromHeroes.isSelectedHeroInFavorite));
    this.loggedIn$ = store.pipe(select(fromAuth.getLoggedIn));
  }

  addToFavorite(heroToAdd: Hero) {
    this.store.dispatch(new favorite.AddHero(heroToAdd));
  }

  removeFromFavorite(heroToRemove: Hero) {
    this.store.dispatch(new favorite.RemoveHero(heroToRemove));
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
