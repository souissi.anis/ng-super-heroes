import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '../models/hero';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;
  @Input() isMobile = false;
  @Input() inFavorite: boolean;
  @Input() loggedIn: boolean;
  @Output() add = new EventEmitter<Hero>();
  @Output() remove = new EventEmitter<Hero>();

  constructor() {}

  ngOnInit() {}

  getThumbnail(hero: Hero) {
    return hero.thumbnail.path + '.' + hero.thumbnail.extension;
  }
}
