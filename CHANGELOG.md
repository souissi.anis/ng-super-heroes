# 1.0.1 (2018-03-05)
### Features
- add IE compatibility
- update README.md
- add CHANGELOG.md
