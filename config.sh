#!/usr/bin/env bash

FILE_LOCAL_CONF="src/environments/local.config"
ENV=$1

FILE_ENV_DEV="src/environments/environment.ts"
FILE_ENV_PROD="src/environments/environment.prod.ts"

if [ ! -f "$FILE_ENV_DEV" ];
then
    cp $FILE_ENV_PROD $FILE_ENV_DEV
fi

if [ -f "$FILE_LOCAL_CONF" ];
then
    echo "Config from file: $FILE_LOCAL_CONF"
    source $FILE_LOCAL_CONF
    sed -i "s/MARVEL_API_PUBLIC_KEY/$MARVEL_API_PUBLIC_KEY/g" $FILE_ENV_DEV
    sed -i "s/MARVEL_API_PRIVATE_KEY/$MARVEL_API_PRIVATE_KEY/g" $FILE_ENV_DEV
    sed -i "s/FIREBASE_API_KEY/$FIREBASE_API_KEY_DEV/g" $FILE_ENV_DEV
    sed -i "s/FIREBASE_AUTH_DOMAIN/$FIREBASE_AUTH_DOMAIN_DEV/g" $FILE_ENV_DEV
    sed -i "s/FIREBASE_DATABASE_URL/$FIREBASE_DATABASE_URL_DEV/g" $FILE_ENV_DEV
    sed -i "s/FIREBASE_PROJECT_ID/$FIREBASE_PROJECT_ID_DEV/g" $FILE_ENV_DEV
    sed -i "s/FIREBASE_STORAGE_BUCKET/$FIREBASE_STORAGE_BUCKET_DEV/g" $FILE_ENV_DEV
    sed -i "s/FIREBASE_MESSAGING_SENDER_ID/$FIREBASE_MESSAGING_SENDER_ID_DEV/g" $FILE_ENV_DEV
else
    case $ENV in
        "dev")
            echo "Config for environment: dev"
            sed -i "s/MARVEL_API_PUBLIC_KEY/$MARVEL_API_PUBLIC_KEY/g" $FILE_ENV_DEV
            sed -i "s/MARVEL_API_PRIVATE_KEY/$MARVEL_API_PRIVATE_KEY/g" $FILE_ENV_DEV
            sed -i "s/FIREBASE_API_KEY/$FIREBASE_API_KEY_DEV/g" $FILE_ENV_DEV
            sed -i "s/FIREBASE_AUTH_DOMAIN/$FIREBASE_AUTH_DOMAIN_DEV/g" $FILE_ENV_DEV
            sed -i "s/FIREBASE_DATABASE_URL/$FIREBASE_DATABASE_URL_DEV/g" $FILE_ENV_DEV
            sed -i "s/FIREBASE_PROJECT_ID/$FIREBASE_PROJECT_ID_DEV/g" $FILE_ENV_DEV
            sed -i "s/FIREBASE_STORAGE_BUCKET/$FIREBASE_STORAGE_BUCKET_DEV/g" $FILE_ENV_DEV
            sed -i "s/FIREBASE_MESSAGING_SENDER_ID/$FIREBASE_MESSAGING_SENDER_ID_DEV/g" $FILE_ENV_DEV
        ;;
        "prod")
            echo "Config for environment: prod"       
            sed -i "s/MARVEL_API_PUBLIC_KEY/$MARVEL_API_PUBLIC_KEY/g" $FILE_ENV_PROD
            sed -i "s/MARVEL_API_PRIVATE_KEY/$MARVEL_API_PRIVATE_KEY/g" $FILE_ENV_PROD
            sed -i "s/FIREBASE_API_KEY/$FIREBASE_API_KEY/g" $FILE_ENV_PROD
            sed -i "s/FIREBASE_AUTH_DOMAIN/$FIREBASE_AUTH_DOMAIN/g" $FILE_ENV_PROD
            sed -i "s/FIREBASE_DATABASE_URL/$FIREBASE_DATABASE_URL/g" $FILE_ENV_PROD
            sed -i "s/FIREBASE_PROJECT_ID/$FIREBASE_PROJECT_ID/g" $FILE_ENV_PROD
            sed -i "s/FIREBASE_STORAGE_BUCKET/$FIREBASE_STORAGE_BUCKET/g" $FILE_ENV_PROD
            sed -i "s/FIREBASE_MESSAGING_SENDER_ID/$FIREBASE_MESSAGING_SENDER_ID/g" $FILE_ENV_PROD
        ;;
        *)
            echo "unknown environment"
        ;;
    esac
fi
